const QuizManager = require('./QuizManager')

const Teacher = (name) => {
  let _name = name;
  
  return {
    get name() {
      return _name;
    },

    set name(newName) {
      _name = newName;
      return _name;
    },

    createQuiz(quizName, questions) {
      return QuizManager.createQuiz(quizName, questions);
    },

    saveQuiz(quiz) {
      return QuizManager.saveQuiz(quiz);
    },

    assignQuiz(quiz, student, className) {
      QuizManager.assignQuizToStudent(quiz, student, className);
    }
  }
}

module.exports = Teacher;
