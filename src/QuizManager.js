const Quiz = require('./Quiz');
const Attempt = require('./Attempt');
const ClassroomManager = require('./ClassroomManager');

const QuizManager = () => {
  let _quizes = {};
  let _assignments = {};

  return {
    get quizes() {
      return Object.values(_quizes);
    },

    getQuizByName(name) {
      return _quizes[name];
    },

    createQuiz(quizName) {
      return Quiz(quizName);
    },

    saveQuiz(quiz) {
      _quizes[quiz.name] = quiz;
    },

    assignQuizToStudent(quiz, student) {
      student.assignQuiz(quiz);
    },

    createNewAttempt(quiz) {
      return Attempt(quiz);
    }
  }
}

module.exports = QuizManager;
