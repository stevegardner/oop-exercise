const test = require('tape');
const Attempt = require('./Attempt');
const Quiz = require('./Quiz');

test('Attempt', t => {
  const quiz = Quiz('testQuiz');
  const question1 = {
    prompt: 'What is the greatest city?',
    choices: [
      'San Diego',
      'Miami',
      'New York'
    ],
    answerIndex: 0
  };
  const question2 = {
    prompt: 'When in Rome...',
    choices: [
      'Do as the Romans do.',
      'Hm. I have never heard of that.'
    ],
    answerIndex: 1
  };

  quiz.addQuestion(question1);
  quiz.addQuestion(question2);

  const attempt = Attempt(quiz);
  attempt.saveAnswer(1);
  attempt.saveAnswer(1);
  t.equals(attempt.gradeAnswers(), 0.50, 'it should grade current grade of the attempt');


  
  t.end();
});
