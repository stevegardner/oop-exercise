const test = require('tape');
const Student = require('./Student');
const Quiz = require('./Quiz');

test.only('Student', t => {
  const student = Student('Brian Fantana');
  const quiz = Quiz('testQuiz');
  const question1 = {
    prompt: 'What is the greatest city?',
    choices: [
      'San Diego',
      'Miami',
      'New York'
    ],
    answerIndex: 0
  };
  const question2 = {
    prompt: 'When in Rome...',
    choices: [
      'Do as the Romans do.',
      'Hm. I have never heard of that.'
    ],
    answerIndex: 1
  };

  quiz.addQuestion(question1);
  quiz.addQuestion(question2);

  student.assignQuiz(quiz);
  t.equals(Object.keys(student.notStartedQuizes).length, 1, 'should have one not started quiz');

  const attempt = student.attemptQuiz(quiz);
  t.equals(attempt.answers.length, 0, 'should have no answers yet');
  t.equals(Object.keys(student.notStartedQuizes).length, 0, 'should have moved the quiz out');
  t.equals(Object.keys(student.inprogressAttempts).length, 1, 'should have moved the attempt to in progress');

  attempt.saveAnswer(0);
  attempt.saveAnswer(1);

  t.equals(Object.keys(student.inprogressAttempts).length, 1, 'attempt is still in progress');
  t.equals(Object.keys(student.completedAttempts).length, 0, 'should have no attempts completed');

  student.saveAttempt(attempt);
  t.equals(Object.keys(student.inprogressAttempts).length, 0, 'attempt is no longer in progress');
  t.equals(Object.keys(student.completedAttempts).length, 1, 'should have 1 attempts completed');

  t.equals(student.totalGrade, 1, 'should have a total grade of 1');

  t.end();
});
