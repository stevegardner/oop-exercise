const Attempt = (quiz) => {
  const _quiz = quiz;
  let _answers = [];

  return {
    get name() {
      return _quiz.name;
    },

    get answers() {
      return _answers;
    },

    getNextQuestion() {
      return _quiz.questions[_answers.length];
    },

    saveAnswer(choice) {
      _answers.push(choice);
    },

    isComplete() {
      return _quiz.questions.length === _answers.length;
    },

    gradeAnswers() {
      const correct = _quiz.questions.reduce((correctAnswers, question, i) => {
        if (question.answerIndex === _answers[i]) {
          correctAnswers += 1;
        }

        return correctAnswers;
      }, 0);

      return correct / _quiz.questions.length;
    }
  }
}

module.exports = Attempt;
