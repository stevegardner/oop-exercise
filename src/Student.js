const omit = require('lodash/omit');
const QuizManager = require('./QuizManager');

const Student = (name) => {
  const _quizManager = QuizManager();
  let _name = name;
  let _quizes = {};
  let _inprogressAttempts = {};
  let _completedAttempts = {};

  return {
    get name() {
      return _name;
    },

    set name(newName) {
      return _newName;
    },

    get notStartedQuizes() {
      return _quizes;
    },

    get inprogressAttempts() {
      return _inprogressAttempts;
    },

    get completedAttempts() {
      return _completedAttempts;
    },

    assignQuiz(quiz) {
      _quizes[quiz.name] = quiz;
    },

    attemptQuiz(quiz) {
      let attempt = _inprogressAttempts[quiz.name];
      
      _quizes = omit(_quizes, quiz.name);

      if (attempt) {
        return attempt;
      }

      attempt = _quizManager.createNewAttempt(quiz);
      _inprogressAttempts[quiz.name] = attempt;

      return attempt;
    },

    saveAttempt(attempt) {
      if (attempt.isComplete()) {
        _completedAttempts[attempt.name] = attempt;
        _inprogressAttempts = omit(_inprogressAttempts, attempt.name);
      }
    },

    get totalGrade() {
      const allAttempts = Object.values(_completedAttempts);
      const total = allAttempts.reduce((total, attempt) => {
        return total += attempt.gradeAnswers();
      }, 0);

      return total / allAttempts.length;
    }
  }
}

module.exports = Student;
