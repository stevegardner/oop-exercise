const test = require('tape');
const Quiz = require('./Quiz');

test('Quiz', t => {
  const quiz = Quiz('testQuiz');

  t.equals(quiz.name, 'testQuiz', 'should set the name');

  const question1 = {
    prompt: 'What is the greatest city?',
    choices: [
      'San Diego',
      'Miami',
      'New York'
    ],
    answerIndex: 0
  };
  const question2 = {
    prompt: 'When in Rome...',
    choices: [
      'Do as the Romans do.',
      'Hm. I have never heard of that.'
    ],
    answerIndex: 1
  }

  quiz.addQuestion(question1);
  quiz.addQuestion(question2);

  t.equals(quiz.questions.length, 2, 'should have 2 questions');
  t.equals(quiz.questions[0], question1, 'should have saved question 1');
  t.equals(quiz.questions[1], question2, 'should have saved question 2');

  t.end();
});
