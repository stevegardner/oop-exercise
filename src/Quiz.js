const Question = require('./Question');

const Quiz = (name) => {
  const _name = name;
  let _questions = [];

  return {
    get name() {
      return _name;
    },

    get questions() {
      return _questions;
    },

    addQuestion(question) {
      _questions.push(question);
      return _questions;
    },

    gradeQuiz() {

    }
  }
};

module.exports = Quiz;
