const test = require('tape');
const Teacher = require('./Teacher');
const Student = require('./Student');

test('Teacher', t => {
  const teacher = Teacher('Ron Burgundy');

  t.equals(teacher.name, 'Ron Burgundy', 'should set the name');

  t.end();


});
