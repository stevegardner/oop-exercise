const test = require('tape');
const Classroom = require('./Classroom');
const Student = require('./Student');
const Teacher = require('./Teacher');

test('Classroom', t => {
  const js101 = Classroom('js101');

  t.equals(js101.name, 'js101', 'should set the classroom name');
  t.equals(Object.keys(js101.students).length, 0, 'should have no students');
  t.equals(Object.keys(js101.teachers).length, 0, 'should have no teachers');

  const wes = Student('Wes Mantooth');
  js101.enrollStudent(wes);
  t.equals(Object.keys(js101.students).length, 1, 'should have 1 student');
  t.equals(js101.students[wes.name], wes, 'should have enrolled Wes Mantooth');

  const ron = Teacher('Ron Burgundy');
  js101.enrollTeacher(ron);
  t.equals(Object.keys(js101.teachers).length, 1, 'should have 1 teacher');
  t.equals(js101.teachers[ron.name], ron, 'should have enrolled Ron Burgundy');
  
  t.end();
});
