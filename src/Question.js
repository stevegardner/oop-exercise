const Question = (prompt, choices, answerIndex) => {
  return {
    prompt,
    choices,
    answerIndex
  }
}

module.exports = Question;
