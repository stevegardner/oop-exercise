const Classroom = (className) => {
    let _name = className;
    let _teachers = {};
    let _students = {};

  return {
    get name() {
      return _name;
    },

    enrollTeacher(teacher) {
      _teachers[teacher.name] = teacher;
    },

    enrollStudent(student) {
      _students[student.name] = student;
    },

    get teachers() {
      return _teachers;
    },

    get students() {
      return _students;
    },

    getStudentByName(studentName) {
      return _students[studentName]
    }
  }
}

module.exports = Classroom;
