const test = require('tape');
const QuizManager = require('./QuizManager');
const ClassroomManager = require('./ClassroomManager');
const Student = require('./Student');

test('Quiz Manager', t => {
  const manager = QuizManager();

  t.equals(manager.quizes.length, 0, 'it should have no quizes');

  const quiz = manager.createQuiz('testQuiz');

  manager.saveQuiz(quiz);

  t.equals(manager.quizes.length, 1, 'it should have 1 quiz');
  t.equals(manager.getQuizByName('testQuiz'), quiz, 'it should get the correct quiz');

  const brick = Student('Brick Tamland');
  
  manager.assignQuizToStudent(quiz, brick);

  t.equals(brick.notStartedQuizes[quiz.name], quiz, 'should have assigned 1 quiz');

  t.end();
});
