const Classroom = require('./Classroom');

const ClassroomManager = () => {
  let _classes = {};

  return {
    get classes() {
      return _classes; 
    },

    getClassByName(name) {
      return _classes[name];
    },

    registerClass(name) {
      if (_classes[name]) return;
      
      const newClass = Classroom(name)
      _classes[name] = newClass;

      return newClass;
    },

    getStudentFromClass(name) {

    }
  }
};

module.exports = ClassroomManager;
