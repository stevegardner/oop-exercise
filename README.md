# You Down with OOP

This project makes no assumptions about a database. Though, in production, it obviously would make far more sense to store entities in a data store rather than in memory. The goal was to stay simple and focus on Object Oriented principles.

## Getting Started

This project uses yarn. You're welcome to use npm.

### Installation

Run either `yarn install` or `npm install` to get the dependencies for running tests. Sticking with a very simple setup, tape is used as the test runner.

### Running the tests

Run `yarn test` or `npm run test` to go through the tests once. Adding `:watch` will keep the tests in watch mode and run them again on each save.
